import gql from 'graphql-tag'

export const LOGIN_USER = gql`
mutation authLogin($loginInout : LoginInput!) {
  authLogin(loginInout: $loginInout) {
    profile{
      firstName
    }
  }
}
`
export const REGISTER_USER = gql`
mutation authRegister($registerInput : RegisterInput!) {
  authRegister(registerInput: $registerInput) {
    profile{
      firstName
    }
  }
}
`
